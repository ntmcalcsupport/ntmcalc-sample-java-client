package se.ntmcalc.ws.client.rs;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;

@Provider
public class ResponseInterceptor implements ReaderInterceptor {

    public Object aroundReadFrom(ReaderInterceptorContext interceptorContext)
            throws IOException, WebApplicationException {
        InputStream inputStream = interceptorContext.getInputStream();

        StringBuilder inputStringBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        String line = bufferedReader.readLine();
        while(line != null){
            inputStringBuilder.append(line);
            line = bufferedReader.readLine();
        }

        String responseContent = inputStringBuilder.toString();
        System.out.println("Response body: " + responseContent);
        interceptorContext.setInputStream(new ByteArrayInputStream(responseContent.getBytes()));
 
        return interceptorContext.proceed();
    }
}
