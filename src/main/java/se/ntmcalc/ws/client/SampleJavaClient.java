package se.ntmcalc.ws.client;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.BearerAuthFilter;
import org.keycloak.admin.client.token.TokenManager;

import se.ntmcalc.ws.client.model.CalculationObject;
import se.ntmcalc.ws.client.model.CalculationRequest;
import se.ntmcalc.ws.client.model.CalculationResponse;
import se.ntmcalc.ws.client.model.ErrorItem;
import se.ntmcalc.ws.client.model.ErrorResponse;
import se.ntmcalc.ws.client.rs.RequestInterceptor;
import se.ntmcalc.ws.client.rs.ResponseInterceptor;

/**
 * Sample Java Client for calling the NTM /transportactivities web service.
 * 
 * @author Tobias Hägglund
 */
public class SampleJavaClient {
	
	private Builder builder = null;

	/**
	 * Initializes the authorization server URL, credentials and /transportactivities web service URL in a JAX-RS Builder object. 
	 */
	private void init() {
		String authServer = "https://auth.transportmeasures.org/auth";
		Keycloak keycloak = Keycloak.getInstance(authServer, "ntm", "my-transport-company", "abcd1234", "my-transport-company", "61dd5708-886d-4258-b35e-4edeeb02820b");
		TokenManager tokenManager = keycloak.tokenManager();
		
		// Verify early that obtaining an access token from NTM authorization server works.
		// Will fail if there are any errors in the credentials above.
		try {
			System.out.println("Access token: " + tokenManager.getAccessTokenString());
		} catch (WebApplicationException e) {
			System.err.println("Error obtaining access token");
			throw e;
		}
		 
		// Create JAX-RS Builder object.
		// The BearerAuthFilter will call getAccessTokenString() and add a HTTP Bearer Authorization header on each call, like so:
		// Authorization: Bearer <accessToken>
		builder = ClientBuilder.newClient()
				.target("https://api.transportmeasures.org/v1/transportactivities")
				.register(RequestInterceptor.class)			// Logs the raw JSON request
				.register(ResponseInterceptor.class)		// Logs the raw JSON response
				.register(new BearerAuthFilter(tokenManager))
				.request()
				.header("Accept-Language", "en-US");
	}
	
	/**
	 * Calls the /transportactivities web service for a specific calculation object.
	 * 
	 * @param id calculation object id
	 * @param version calculation object version
	 */
	public void calculateTransportActivities(String id, String version) {
		if (builder == null) {
			init();
		}
		
		// Create request object
		CalculationRequest calculationRequest = new CalculationRequest();
		CalculationObject calculationObject = new CalculationObject();
		calculationObject.setId(id);
		calculationObject.setVersion(version);
		calculationRequest.setCalculationObject(calculationObject);
		 
	    try {
			System.out.println(">>>>> Calling /transportactivities ");
			
			// Call the /transportactivities web service
			CalculationResponse response =
					builder.post(Entity.entity(calculationRequest, MediaType.APPLICATION_JSON), CalculationResponse.class);

			// Print the response
			System.out.println("Returned calculation object: "
					+ "id: " + response.getCalculationObject().getId() + ", "
					+ "version: " + response.getCalculationObject().getVersion());
			
			System.out.println("<<<<< ");
		} catch (WebApplicationException e) {
			Response response = e.getResponse();
			System.err.println("HTTP status: " + response.getStatus() + " " + response.getStatusInfo());
			if (response.hasEntity()) {
				ErrorResponse errorResponse = response.readEntity(ErrorResponse.class);
				System.err.println("Got error, code=" + errorResponse.getCode());
				for (ErrorItem error : errorResponse.getErrors()) {
					System.err.println(error.getMessage());
				}
			}
		}
	}

	/**
	 * Main method creating a sample java client and calling web service.
	 */
	public static void main(String[] args) {
		SampleJavaClient sampleJavaClient = new SampleJavaClient();
		
		try {
			sampleJavaClient.calculateTransportActivities("rigid_truck_7_5_t", "1");
			sampleJavaClient.calculateTransportActivities("city_bus", "1");
		} catch (Exception e) {
			System.err.println("Error calculating transport activities");
			e.printStackTrace();
		}
	}
}
