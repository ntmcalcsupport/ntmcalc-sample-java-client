package se.ntmcalc.ws.client.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Parameter {
	
	private String name;
	private String id;
	private String value;
	private String type;
	private String origin;
	private RangeConstraint rangeConstraint;
	private boolean readOnly = false;
	private List<OptionValue> optionValues;

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getOrigin() {
		return origin;
	}
	
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	
	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public RangeConstraint getRangeConstraint() {
		return rangeConstraint;
	}

	public void setRangeConstraint(RangeConstraint rangeConstraint) {
		this.rangeConstraint = rangeConstraint;
	}

	public List<OptionValue> getOptionValues() {
		return optionValues;
	}
	
	public void setOptionValues(List<OptionValue> optionValues) {
		this.optionValues = optionValues;
	}
}
