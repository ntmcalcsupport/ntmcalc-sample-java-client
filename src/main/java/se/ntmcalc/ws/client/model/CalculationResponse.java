package se.ntmcalc.ws.client.model;

import java.util.List;

public class CalculationResponse {
	private CalculationObjectResponse calculationObject;
	private List<Parameter> parameters;
	private List<Message> messages;
	private ResultTable resultTable;

	public CalculationObjectResponse getCalculationObject() {
		return calculationObject;
	}

	public void setCalculationObject(CalculationObjectResponse calculationObject) {
		this.calculationObject = calculationObject;
	}

	public List<Parameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public ResultTable getResultTable() {
		return resultTable;
	}

	public void setResultTable(ResultTable resultTable) {
		this.resultTable = resultTable;
	}
}
