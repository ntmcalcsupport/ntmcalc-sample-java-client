package se.ntmcalc.ws.client.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RangeConstraint {
	
	private Integer gt = null;
	private Integer gte = null;
	private Integer lt = null;
	private Integer lte = null;
	
	public Integer getGt() {
		return gt;
	}
	
	public void setGt(Integer gt) {
		this.gt = gt;
	}
	
	public Integer getGte() {
		return gte;
	}
	
	public void setGte(Integer gte) {
		this.gte = gte;
	}
	
	public Integer getLt() {
		return lt;
	}
	
	public void setLt(Integer lt) {
		this.lt = lt;
	}
	
	public Integer getLte() {
		return lte;
	}

	public void setLte(Integer lte) {
		this.lte = lte;
	}
}
