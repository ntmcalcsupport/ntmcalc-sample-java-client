package se.ntmcalc.ws.client.model;

import java.util.List;

public class ErrorWrapper {
	private int code;
	private List<ErrorItem> errors;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public List<ErrorItem> getErrors() {
		return errors;
	}

	public void setErrors(List<ErrorItem> errors) {
		this.errors = errors;
	}
}
