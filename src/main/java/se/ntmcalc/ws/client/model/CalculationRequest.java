package se.ntmcalc.ws.client.model;

public class CalculationRequest {
	private CalculationObject calculationObject;

	public CalculationObject getCalculationObject() {
		return calculationObject;
	}

	public void setCalculationObject(CalculationObject calculationObject) {
		this.calculationObject = calculationObject;
	}
}
